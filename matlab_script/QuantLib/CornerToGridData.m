function [crop_values, crop_weight, full_weight] = CornerToGridData(data, weight, corners, field)
% CornerToGridData Use corner coordinates to get corresponding grid values
% and weight of a specific field
%   

% ulc = [250 300];
% brc = [600 1000];
% field = "phi";
% 
% path = "/home/stephane/Documents/Projects/PKhalilian/Anisotropy/transversal/wt1_tr";
% load(path + filesep + "Results.mat");
% data = Results;
% 
% weight = ones(1,data.numY,data.numX);

ulc = corners(:, 1, :);
brc = corners(:, 2, :);

% get linear index of boxes of interest
grid_idx_y = data.Posi(:,2) > ulc(1) & data.Posi(:,2) < brc(1);
grid_idx_x = data.Posi(:,1) > ulc(2) & data.Posi(:,1) < brc(2);
grid_idx = find(grid_idx_x & grid_idx_y);
[y, x] = ind2sub([data.numY data.numX], grid_idx);

values = data.im_regav.(field);
crop_values = squeeze(values(:, min(y):max(y), min(x):max(x)));
crop_weight = squeeze(weight(:, min(y):max(y), min(x):max(x)));

full_weight = zeros(size(squeeze(weight)));
full_weight(min(y):max(y), min(x):max(x)) = squeeze(weight(:, min(y):max(y), min(x):max(x)));

end

