function Analyse(folder, quantities, phenotype)
% Quantification extract quantities and weight from processed folder
%   and return cropped area to compared against.

%% Extract list of subfolders to process
listing = dir(folder);
listing(strncmp({listing.name}, '.', 1)) = [];
Folders = listing([listing.isdir]);

%% Process all animals
for t = 1:numel(phenotype)
    TF = contains({Folders.name}, phenotype{t});
    subFolders = Folders(TF);
    for f = 1:numel(subFolders)
        folder_name = [subFolders(f).folder filesep subFolders(f).name];
        if ~exist([folder_name filesep 'QuantifBackup.mat'])
            Quantification(folder_name, quantities, phenotype{t});
        end
    end
end

%% Reload backup
qCount = 1;
for t = 1:numel(phenotype)
    TF = contains({Folders.name}, phenotype{t});
    subFolders = Folders(TF);
    for f = 1:numel(subFolders)
        folder_name = [subFolders(f).folder filesep subFolders(f).name];
        BackupList(qCount) = load([folder_name filesep 'QuantifBackup.mat']);
        qCount = qCount + 1;
    end
end

%% quantification reshape
quantifications = GetQuantities(BackupList);
writetable(quantifications, [folder filesep 'globalQuantification.csv']);

% % Plot analysis
% for q=1:numel(quantities)
%     PlotValueAlongAxis(BackupList, phenotype, folder, quantities{q}, true);
%     PlotValueHistogram(BackupList, phenotype, folder, quantities{q}, true);
% end

end