function PlotValueHistogram(BackupList, phenotype, folder, quantity, csv)

colors = [[128 193 219]./255 ; [243 169 114]./255];
boxsize = (128*0.5) * 0.313;

tau1 = 0.25;
tau2 = -0.1;

for f = 1:numel(phenotype)
    
    qmerge_a1 = [];
    qmerge_a2 = [];
    
    for b = 1:numel(BackupList)
        
        if strcmp(BackupList(b).QuantifBackup.phenotype, phenotype(f))
            qvalues_a1 = BackupList(b).QuantifBackup.area1.(quantity);
            qvalues_a2 = BackupList(b).QuantifBackup.area2.(quantity);
            qweigth_a1 = BackupList(b).QuantifBackup.area1.weight;
            qweigth_a2 = BackupList(b).QuantifBackup.area2.weight;
            
            if strcmp(quantity, 'angS')
                qvalues_a1 = qvalues_a1 + (pi/2);
                qvalues_a2 = qvalues_a2 + (pi/2);
                qvalues_a1(BackupList(b).QuantifBackup.area1.S>tau2) = nan;
                qvalues_a2(BackupList(b).QuantifBackup.area1.S>tau2) = nan;
            end
            if strcmp(quantity, 'S')
                qvalues_a1 = abs(qvalues_a1);
                qvalues_a2 = abs(qvalues_a2);
            end
            
            qvalues_a1(qweigth_a1<tau1) = nan;
            qvalues_a2(qweigth_a2<tau1) = nan;
            
            qmerge_a1 = [qmerge_a1 ; qvalues_a1(:)];
            qmerge_a2 = [qmerge_a2 ; qvalues_a2(:)];
        end
        
    end % end backup
    
    
    f1 = figure();
    if strcmp(quantity, 'angS')
        polarhistogram(qmerge_a1, [0:(pi/20):pi],  'FaceColor', colors(f,:), 'FaceAlpha',.3);
    else
        histogram(qmerge_a1, [0:(1/20):1],  'FaceColor', colors(f,:), 'FaceAlpha',.3);
    end
    title(['Histogram ' quantity ' ' phenotype{f} ' Area 1'])
    saveas(f1, [folder filesep 'histogram_' quantity '_' phenotype{f} '_Area=1.png']);
    close(f1);
    if csv
        csvwrite([folder filesep 'histogram_' quantity '_' phenotype{f} '_Area=1.csv'], qmerge_a1);
    end
    
    f1 = figure();
    if strcmp(quantity, 'angS')
        polarhistogram(qmerge_a2, [0:(pi/20):pi],  'FaceColor', colors(f,:), 'FaceAlpha',.3);
    else
        histogram(qmerge_a2, [0:(1/20):1],  'FaceColor', colors(f,:), 'FaceAlpha',.3);
    end
    title(['Histogram ' quantity ' ' phenotype{f} ' Area 2'])
    saveas(f1, [folder filesep 'histogram_' quantity '_' phenotype{f} '_Area=2.png']);
    close(f1);
    if csv
        csvwrite([folder filesep 'histogram_' quantity '_' phenotype{f} '_Area=2.csv'], qmerge_a2);
    end
    
end % phenotype





end