function QuantifBackup = Quantification(folder, field_list, phenotype)
% Quantification extract quantities and weight from processed folder
%   and return cropped area to compared against.

% folder = 'E:\UserData\srigaud\script\wt1_tr';
% field_list = {"phi"; "angS"};

% initialise output
QuantifBackup = struct();

%% clean and select files that are going to be processed
[~,animal,~] = fileparts(folder);
listing = dir(folder);
mat_file_ind = cellfun(@(x)( ~isempty(x) ), regexpi({listing.name}, 'Results.mat'));
ph_file_ind = cellfun(@(x)( ~isempty(x) ), regexpi({listing.name}, 'C4'));
sox_file_ind = cellfun(@(x)( ~isempty(x) ), regexpi({listing.name}, 'C3'));

%% load data in workspace for processing
load([folder filesep listing(mat_file_ind).name]);
phal = imread([folder filesep listing(ph_file_ind).name]);
sox9 = imread([folder filesep listing(sox_file_ind).name]);

%% compute region of interest for both signal
[roi_phal, roi_sox9] = GetRoI(phal, sox9);

imwrite(uint8(roi_phal),[folder filesep 'roi_phal_image.png']);
imwrite(uint8(roi_sox9),[folder filesep 'roi_sox9_image.png']);

%% user input for area definition
img = cat(3, imadjust(phal), imadjust(sox9), zeros(size(phal)));
[corners, figHandler] = ZoneSelectionInterface(img, roi_phal, Results);
saveas(figHandler, [folder filesep 'QuantifSelectedArea.png'])
close(figHandler);

%% compute weight from region of interest and grid configuration
[w1_phal, w2_sox9, w3_all] = ComputeWeight(Results, roi_phal, roi_sox9);

imwrite((squeeze(w1_phal)),[folder filesep 'weight_phal_image.png']);
imwrite((squeeze(w2_sox9)),[folder filesep 'weight_sox9_image.png']);
imwrite((squeeze(w3_all)),[folder filesep 'weight_all_image.png']);

%% compute intensity
Results = ComputeIntensity(sox9, Results, 128);


%% extract values and weights and fill struct
QuantifBackup.corners = corners;
QuantifBackup.roi_phal = roi_phal;
QuantifBackup.roi_sox9 = roi_sox9;
QuantifBackup.phenotype = phenotype;
QuantifBackup.name = animal;

for n=1:numel(field_list)
    % using area information, crop value and weight from analysis
    [crop_values_area1, crop_weight_area1, full_w_1] = CornerToGridData(Results, w1_phal, corners(1,:,:), field_list{n});
    [crop_values_area2, crop_weight_area2, full_w_2] = CornerToGridData(Results, w2_sox9, corners(2,:,:), field_list{n});
    [crop_values_area3, crop_weight_area3, full_w_3] = CornerToGridData(Results, w3_all, corners(3,:,:), field_list{n});


    % store in struct for futur plotting
    QuantifBackup.area1.weight = crop_weight_area1;
    QuantifBackup.area1.(field_list{n}) = crop_values_area1;
    QuantifBackup.area2.weight = crop_weight_area2;
    QuantifBackup.area2.(field_list{n}) = crop_values_area2;
    QuantifBackup.area3.weight = crop_weight_area3;
    QuantifBackup.area3.(field_list{n}) = crop_values_area3;
end

    imwrite((squeeze(full_w_1)),[folder filesep 'weight_phal_image.png']);
    imwrite((squeeze(full_w_2)),[folder filesep 'weight_sox9_image.png']);
    imwrite((squeeze(full_w_3)),[folder filesep 'weight_all_image.png']);
    
    load([folder filesep 'Results.mat']);
    gridout = zeros(size(phal));
    wsize = 64;
    for r = 1:length(Results.Posi)
        yulc = Results.Posi(r, 2);
        xulc = Results.Posi(r, 1);
        gridout(yulc,xulc:xulc+wsize) = 255;
        gridout(yulc+wsize,xulc:xulc+wsize) = 255;
        gridout(yulc:yulc+wsize,xulc) = 255;
        gridout(yulc:yulc+wsize,xulc+wsize) = 255;
    end
    imwrite(gridout,[folder filesep 'grid_image.png']);
    

%% save output
save([folder filesep 'QuantifBackup.mat'], 'QuantifBackup')

end

