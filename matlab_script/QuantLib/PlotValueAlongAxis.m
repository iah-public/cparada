function PlotValueAlongAxis(BackupList, phenotype, folder, quantity, csv)

colors = [[128 193 219]./255 ; [243 169 114]./255];
boxsize = (128*0.5) * 0.313;
tau1 = 0.25;
tau2 = -0.1;

vnames = {'Type';'Animal';'ValueType';'Distance';'Value'};
quantifications = table('Size',[0 5],'VariableTypes', {'string';'string'; 'string'; 'double'; 'double'},'VariableNames',vnames);

Phenotype = {};
Animal = {};
ValueType = {};
Distance = [];
Value = [];

f1=figure();
for f = 1:numel(phenotype)
    
    projValue = nan(50, 100);
    maximum_size = -1;
    count = 1;
    
    for b = 1:numel(BackupList)
        
        

        if strcmp(BackupList(b).QuantifBackup.phenotype, phenotype(f))
            

            
            qvalues = BackupList(b).QuantifBackup.area3.(quantity);
            if strcmp(quantity, 'angS')
                qvalues = qvalues + (pi/2);
                qvalues(BackupList(b).QuantifBackup.area3.S>tau2) = nan;
                qvalues(qvalues>(pi/2)) = (pi/2) - (qvalues(qvalues>(pi/2)) - (pi/2));
                qvalues = rad2deg(qvalues);
            end
            if strcmp(quantity, 'S')
                qvalues = abs(qvalues);
            end
            qweigth = BackupList(b).QuantifBackup.area3.weight;
            qvalues(qweigth<tau1) = nan;
            values_size = size(qvalues, 1);
            for i=1:values_size
                avg_values(i) = nansum(qvalues(i,:) .* qweigth(i,:)) / sum(~isnan(qvalues(i,:)));
            end
            projValue(count, 1:values_size) = avg_values;
            maximum_size = max(maximum_size, values_size);
            count = count + 1;
            clear( 'qvalues', 'qweigth', 'avg_values');
            
            Phenotype = {Phenotype ; BackupList(b).QuantifBackup.phenotype};
            Animal = {Animal ; BackupList(b).QuantifBackup.name};
            ValueType = {ValueType ; quantity};
            Distance = [Distance ];
            Value = [Value ];
        end
        
    end % backup loop
    
    
    
    xvals = [0:maximum_size-2] * boxsize;
    qplot = projValue(1:count-1,1:maximum_size-1);
    
    hold on
    options.handle=f1;
    options.color_area = colors(f,:);
    options.color_line = colors(f,:);
    options.alpha = 0.5;
    options.line_width = 2;
    options.error = 'std';
    options.x_axis = xvals;
    plot_areaerrorbar(qplot, options)
    hold off
    
    if csv
        csvwrite([folder filesep quantity '_' phenotype{f} '_D-P_axis.csv'], qplot)
    end
    
end % phenotype loop

if strcmp(quantity, 'angS')
    ylim([0 90])
end
% xlim([0 500])


legend(phenotype)
xlabel("D-P axis length (um)")
ylabel(quantity)
title([quantity ' D-P Axis'])
saveas(f1, [folder filesep quantity '_D-P_Axis.png']);
close(f1);


end

