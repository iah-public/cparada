function Results = ComputeIntensity(image, Results, boxsize)

% image = double(imread('Z:\SRigaud\PKhalilian\Anisotropy\sagitarial_d12.5\wt2_sg\MAX_C3-wt_4x4_40x_z1_A2_x2 _3_long_Stitch-1.tif'));
% load('Z:\SRigaud\PKhalilian\Anisotropy\sagitarial_d12.5\wt2_sg\Results.mat');
% boxsize=128;

imageSize = size(image);
boxArea = (boxsize+1)*(boxsize+1);
gridPixelLs = NaN(length(Results.Posi), boxArea);
for b = 1:length(Results.Posi)
   ulc = Results.Posi(b,:);
   [X, Y] = meshgrid(ulc(1):ulc(1)+boxsize,ulc(2):ulc(2)+boxsize);
   coord = sub2ind(imageSize,Y,X);
   gridPixelLs(b, :) = coord(:)';
end


% normalise between 0-1
image = double(image);
image = (image - nanmin(image(:))) * (1 ./ (nanmax(image(:)) - nanmin(image(:))));
intensityDistribution = sum(image(gridPixelLs),2) / boxArea; % 1.0
intensityDistribution = reshape(intensityDistribution, Results.numY, Results.numX);

Results.im_regav.Intensity = reshape(intensityDistribution, [1, Results.numY, Results.numX]);

end
