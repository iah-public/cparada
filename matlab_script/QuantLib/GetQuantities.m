function quantifications = GetQuantities(BackupList)

qnames = {'angS' ; 'S' ; 'Intensity'};
anames = {'area1' ; 'area2'; 'area3'};

vnames = {'Type';'Animal';'Area';'ValueType';'Distance';'Value';'Weight'};
Phenotype = cell(1,0);
Animal = cell(1,0);
Area = cell(1,0);
ValueType = cell(1,0);
Distance = [];
Value = [];
Weight = [];

Intensity = [];
Angle = [];
Shear = [];
SymAngle = [];

for b = 1:numel(BackupList)
    currentBkp = BackupList(b).QuantifBackup;
    for a = 1:numel(anames)
        for q = 1:numel(qnames)
            currentArea = anames{a};
            currentVal = qnames{q};
            
            qvalues = currentBkp.(currentArea).(currentVal);
            
            
            
            if strcmp(currentVal, 'Intensity')
                Intensity = [Intensity; qvalues(:)];
            end
            if strcmp(currentVal, 'angS')
                qvalues = qvalues + (pi/2);
                Angle = [Angle; qvalues(:)];
                qvalues(qvalues>(pi/2)) = (pi/2) - (qvalues(qvalues>(pi/2)) - (pi/2));
                SymAngle = [SymAngle; qvalues(:)];
            end
            if strcmp(currentVal, 'S')
                qvalues = abs(qvalues);
                Shear = [Shear; qvalues(:)];
            end
            
            
        end
        
        qweigth = currentBkp.(currentArea).weight;
        
        [h, w] = size(qweigth);
        [~, YY] = meshgrid(1:w,1:h);
        
        Distance = [Distance; YY(:)];
        Weight = [Weight; qweigth(:)];
        
        tmp = cell(1, length(qvalues(:)));
        tmp(:) = {num2str(a)};
        Area = [Area; tmp(:)];
        tmp(:) = {currentBkp.phenotype};
        Phenotype = [Phenotype; tmp(:)];
        tmp(:) = {currentBkp.name};
        Animal = [Animal; tmp(:)];
    end
end

length(Phenotype)
length(Animal)
length(Area)
length(Distance)
length(Intensity)
length(Angle)
length(SymAngle)
length(Shear)
length(Weight)

quantifications = table(Phenotype, Animal, Area, Distance, Intensity, Angle, SymAngle, Shear, Weight);
