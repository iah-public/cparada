function [roi_ph, roi_sox] = GetRoI(ph, sox)
% GetRoI compute and return binary map of phaloidine and sox9 localisation
%   in image.
%   ph, sox, respectively phaloiding and sox9 data.
%

ph = double(ph);
sox = double(sox);

% normalise [0-1] and convert to double
roi_ph = (ph - min(ph(:))) / (max(ph(:)) - min(ph(:)));
roi_sox = (sox - min(sox(:))) / (max(sox(:)) - min(sox(:)));

% get roi of sox9
sigma = 25;
h = fspecial('gaussian', round(3*sigma), sigma);
roi_sox = imfilter(roi_sox, h, 'same');
treshold = graythresh(roi_sox);
roi_sox = imbinarize(roi_sox, treshold);

CC = bwconncomp(roi_sox);
numPixels = cellfun(@numel,CC.PixelIdxList);
[biggest, idx] = max(numPixels);
roi_sox = zeros(size(roi_sox));
roi_sox(CC.PixelIdxList{idx}) = 1;
roi_sox = bwconvhull(roi_sox);

% get roi of ph
radius = 50;
roi_ph = medfilt2(roi_ph, [3 3]);
roi_ph(roi_ph > 0) = 1;
roi_ph = imfill(roi_ph, 'holes');
roi_ph = imerode(roi_ph, strel('disk', radius));

roi_ph = roi_ph - roi_sox;

double(roi_ph);
double(roi_sox);

end

