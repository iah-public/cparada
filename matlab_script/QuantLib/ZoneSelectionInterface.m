function [corners, interfaceHandle] = ZoneSelectionInterface(img, roi, data)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

% path = "/home/stephane/Documents/Projects/PKhalilian/Anisotropy/transversal/wt1_tr";
% a = imread(path + filesep +"C4-control_sox9_phalloidin_3x3_1x_b2_overlapping_Image_21_Stitch.tif");
% b = imread(path + filesep +"C2-control_sox9_phalloidin_3x3_1x_b2_overlapping_Image_21_Stitch.tif");
% img = cat(3, a, b, zeros(size(a)));
%
% load(path + filesep + "Results.mat");
% data = Results;

% initialisation
corners = nan(2, 2, 2);

% visualisation interface
screenSize = get(0, 'ScreenSize');
Left = 5;
Bottom = 45;
Width = screenSize(3)/2;
Height = screenSize(4) - 126;
interfaceHandle = figure('Name','Define Areas to Analyse.');
set(interfaceHandle, 'Units', 'pixels');
set(interfaceHandle, 'WindowStyle', 'modal');
set(interfaceHandle, 'Position', [Left Bottom Width Height]);

Areas.roi = roi;
Areas.show = true;

% zone managment loop
zone = 1;
while zone < 3
    % update display with text description and current selected zone and
    % roi and other stuff
    UpdateDisplay(img, corners, zone, Areas);
    set(interfaceHandle, 'Position', [Left Bottom Width Height]);
    
    % action managment loop
    clicks = 1;
    while clicks < 3
        % Get graphic input action
        try
            [xi, yi, buttemp] = ginputWhite(1);
        catch err
            if strcmp(err.identifier,'MATLAB:ginput:FigureDeletionPause')
                disp(err.identifier); % other error
            end
            return;
        end
        % Input action managment
        switch buttemp
            case 1 % Mouse-L-Click : select a cell
                corners(zone, clicks, :) = [round(yi) round(xi)];
                clicks = clicks + 1;
            case 9 % tab
                Areas.roi = ~Areas.roi;
            case 27 % esc : exit interface
                button = questdlg('Do you want to exit the interface?','Exit','Yes','No','Yes');
                if strcmp(button, 'Yes')
                    return
                end
        end % end input managment
        UpdateDisplay(img, corners, zone, Areas);
    end % end clicks loop
    
    % User zone validation
    if zone == 2
        button = questdlg('Continue with those Areas?','Area validation','Yes','No','Yes');
        if strcmp(button, 'No')
            corners(:, :, :) = nan;
            zone = 0;
        end
    end
    
    % loop increment
    zone = zone + 1;
end % end zone loop

UpdateDisplay(img, corners, [], Areas);
corners(3,1,1) = min(corners(1:2,1,1));
corners(3,1,2) = min(corners(1:2,1,2));
corners(3,2,1) = max(corners(1:2,2,1));
corners(3,2,2) = max(corners(1:2,2,2));
% all should be good
end

function UpdateDisplay(img, corners, zone, Areas)
% reset display
cla reset;

% plot background image
imshow(img, [], 'Border', 'tight');

if Areas.show
    hold on
    h = imshow(Areas.roi);
    set(h, 'AlphaData', ones(size(Areas.roi))*0.5);
    hold off
end

% plot text on upper left corner of the image for legend and info
if ~isempty(zone)
    hold on
    text(15, 15, ['Define corners for area #' num2str(zone)], "color", "yellow", "FontSize", 18, 'FontWeight','bold')
    hold off
end

% plot all current clicks with yellow markers
hold on
plot(corners(:,:,2), corners(:,:,1), '+y', "markersize", 10);
hold off

% plot area if both corner are defined
for z = 1:2
    if all(~isnan(corners(z, :)))
        x_list = [corners(z, 1, 2) corners(z, 2, 2) corners(z, 2, 2) corners(z, 1, 2) corners(z, 1, 2)];
        y_list = [corners(z, 1, 1) corners(z, 1, 1) corners(z, 2, 1) corners(z, 2, 1) corners(z, 1, 1)];
        hold on
        plot(x_list, y_list, '-c', 'linewidth', 2);
        text(round(mean(x_list(1:end-1))) - 50, round(mean(y_list(1:end-1))), ['Area = ' num2str(z)], "color", "cyan", "FontSize", 14, 'FontWeight','bold')
        hold off
    end
end

end
