function [w1, w2, w3] = ComputeWeight(data, roi1, roi2)
% ComputeWeight Using grid information and both roi, we compute weight (0-1)
%   for each compartment of the grid
%

% output container initialisation
w1 = zeros(1, data.numY, data.numX);
w2 = zeros(1, data.numY, data.numX);
w3 = zeros(1, data.numY, data.numX);

roi3 = or(roi1, roi2);

% grid box size
dS = median(abs(data.Posi(1:data.numY-1, 2) - data.Posi(2:data.numY, 2)));

% loop on each grid box
for b = 1:data.regl
    % get coordinate of the grid in the data
    [ny, nx] = ind2sub([data.numY data.numX], b);
    
    % get the box coordinatess
    x1 = round(data.Posi(b, 1));
    y1 = round(data.Posi(b, 2));
    [X, Y] = meshgrid(x1:x1+dS, y1:y1+dS);
    coord = sub2ind(size(roi1), Y, X);
    
    % get roi values
    crop_roi1 = roi1(coord);
    crop_roi2 = roi2(coord);
    crop_roi3 = roi3(coord);
    
    % compute weight
    w1(1, ny, nx) = sum(crop_roi1(:)) / numel(crop_roi1(:));
    w2(1, ny, nx) = sum(crop_roi2(:)) / numel(crop_roi2(:));
    w3(1, ny, nx) = sum(crop_roi3(:)) / numel(crop_roi3(:));
end

end

