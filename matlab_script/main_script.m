
%% Sanity clear (avoid workspace error)
clear all;
close all;
clc;

%% Add folder to path
addpath('QuantLib')

%% Input folder
input_folder = '../analysis_2021/sagitarial_d12.5';

%% Phenotype list
% keywords in folder identifying the experiment phenotype
phenotype = { ...  % folder keyword to identify various phenotype
    'wt' ; ...
    'mu' ; ...
    };

%% Quantities to extract
% comment or uncomment line to add/remove from extraction
quantities = { ... 
    'S'    ; ...                % anisotropy amplitude
    'angS' ; ...                % anisotropy angle [0:pi]
    'Intensity'    ; ...        % average intensity 
    };

%% Run analysis of pipeline quantification
Analyse(input_folder, quantities, phenotype);
