# Mechanical feedback defines organizing centers to drive digit emergence

## Description

This repository contain code used in the **Mechanical feedback defines organizing centers to drive digit emergence** study.
It is an extention of the [(Durande et. al., 2019)](https://github.com/mdurande/coarse-grained-anisotropy-and-size-using-FFT) framework to incorporate spatiality in the anisotropy (orientation and strain) of cells.
This is applied for both study of cell anysotropy in Sox9(+) areas against Sox9(-) areas of the digit during formation in Wnt5a-/- and Ctrl individuals.

![image](./figure/description.png)

## Usage

The matlab script (2019b) take as input the a list of processed data from Durande's framework and display simplistic UI for region delimitation.
The output is a formated CSV file containing the Orientation, Shear, and Intensity per region of tissues.

The CSV file is the direct input for the notebook script to plots orientation histograms and average distribution along the Proximal-Distal axis.


```
conda create -n project python=3.9
conda activate project
pip install -r requirements.txt
```

## Acknoledgement

If using or relying on this work, please cite the **Mechanical feedback defines organizing centers to drive digit emergence** paper.

```
// todo: add citation bib
```
