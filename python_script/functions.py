import numpy as np
import pandas as pd

def compute_myosine_orientation(df, boxSize, pixelsize, overlap):
    df = df[df["Area"] == 3]
    data = df.groupby(["Phenotype","Animal","Distance"]).mean()
    data.reset_index(inplace=True)
    data['Distance'] = np.round(data['Distance'] * (boxSize * overlap) * pixelsize)
    data['SymAngleDeg'] = data['SymAngle'].transform(np.rad2deg)   
    data['AngleDeg'] = data['Angle'].transform(np.rad2deg)  

    data.drop(data[data['Distance'] > 300].index, inplace = True)
    return data

def rose_plot(ax, angles, bins=18, density=None, offset=0, lab_unit="degrees",
              start_zero=False, color=None, lw=1, alpha=1, fill=False, **param_dict):
    """
    Plot polar histogram of angles on ax. ax must have been created using
    subplot_kw=dict(projection='polar'). Angles are expected in radians.
    """
    # Wrap angles to [-pi, pi)
    angles = (angles + np.pi) % (2 * np.pi) - np.pi

    # Set bins symetrically around zero
    if start_zero:
        # To have a bin edge at zero use an even number of bins
        if bins % 2:
            bins += 1
        bins = np.linspace(-np.pi, np.pi, num=bins + 1)

    # Bin data and record counts
    count, bin = np.histogram(angles, bins=bins)

    # Compute width of each bin
    widths = np.diff(bin)

    # By default plot density (frequency potentially misleading)
    if density is None or density is True:
        # Area to assign each bin
        area = count / angles.size
        # Calculate corresponding bin radius
        radius = (area / np.pi) ** .5
    else:
        radius = count

    # Plot data on ax
    ax.bar(bin[:-1], radius, zorder=1, align='edge', width=widths,
           edgecolor=color, fill=fill, linewidth=lw, alpha=alpha)

    # Set the direction of the zero angle
    # ax.set_theta_offset(offset)
    ax.set_theta_zero_location('E', offset=offset)

    # Remove ylabels, they are mostly obstructive and not informativemy_yticks
    ax.set(ylim=(0, 0.3))
    ax.set(xlim=(0,np.pi))
    # ax.set_yticks([])
    ax.set_yticks([0,0.1,0.2,0.3])
    ax.set_xticks([0, np.pi/6, np.pi/3, np.pi/2, np.pi-(np.pi/3), np.pi-(np.pi/6), np.pi])
    ax.tick_params(axis='y', which='major')#, labelsize=20)

    if lab_unit == "radians":
        label = ['$0$', r'$\pi/4$', r'$\pi/2$', r'$3\pi/4$',
                 r'$\pi$', r'$5\pi/4$', r'$3\pi/2$', r'$7\pi/4$']
        ax.set_xticklabels(label)